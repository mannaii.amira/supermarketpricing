package fr.sgcib.exercice.kata.supermarketPricing;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fr.sgcib.exercice.kata.supermarketPricing.business.StandardPricingStrategy;
import fr.sgcib.exercice.kata.supermarketPricing.business.VolumeDiscountPricingStrategy;
import fr.sgcib.exercice.kata.supermarketPricing.model.LineItem;
import fr.sgcib.exercice.kata.supermarketPricing.model.Order;
import fr.sgcib.exercice.kata.supermarketPricing.model.StockItem;
import fr.sgcib.exercice.kata.supermarketPricing.business.FreeSamplePricingStrategy;
import fr.sgcib.exercice.kata.supermarketPricing.business.IPricingStrategyFacory;
import fr.sgcib.exercice.kata.supermarketPricing.business.PricingStrategyCreator;
import fr.sgcib.exercice.kata.supermarketPricing.constants.*;

public class SupermarketPricingTest {
	
	@Rule 
	public ExpectedException exceptedException = ExpectedException.none();
	
	@Test
	public void throwsOnInvalidPricingStrategyType() {
		exceptedException.expect(IllegalArgumentException.class);
		exceptedException.expectMessage("Strategy type not found!");
		IPricingStrategyFacory pricingFactory = new PricingStrategyCreator();
		pricingFactory.createPricingStrategy(PricingStrategy.PERCENTAGE_DISCOUNT);
	}
	
	@Test
	public void throwsOnInvalidUnitPrice() {
		exceptedException.expect(IllegalArgumentException.class);
		exceptedException.expectMessage("Unit price is invalid: -2");
		MonetaryAmount unitPrice = Monetary.getDefaultAmountFactory().setNumber(-2).setCurrency("EUR").create();
		new StockItem("USB iphone stand", unitPrice);
	}
	
	@Test
	public void throwsOnInvalidQuantity() {
		exceptedException.expect(IllegalArgumentException.class);
		exceptedException.expectMessage("Number of units is invalid: -7");
		MonetaryAmount unitPrice = Monetary.getDefaultAmountFactory().setNumber(1.99).setCurrency("EUR").create();
		StockItem stockItem = new StockItem("USB iphone stand", unitPrice);
		new LineItem(-7, stockItem);
	}
	
	@Test
	public void makeUnitPricingStrategyTest() {
		Order order = new Order();
		MonetaryAmount unitPrice = Monetary.getDefaultAmountFactory().setNumber(1.99).setCurrency("EUR").create();
		StockItem stockItem = new StockItem("USB iphone stand", unitPrice);
		LineItem item = new LineItem(7, stockItem);
		order.getOrderItems().add(item);
		
		IPricingStrategyFacory pricingFactory = new PricingStrategyCreator();
		pricingFactory.createPricingStrategy(PricingStrategy.UNIT);
		item.getTotalPrice(new StandardPricingStrategy());
		assertThat(item.getTotalPrice().toString(), is("EUR 13.93"));	
	}
	
	@Test
	public void makeFreeSamplePricingStrategyTest() {
		Order order = new Order();
		MonetaryAmount unitPrice = Monetary.getDefaultAmountFactory().setNumber(0.65).setCurrency("EUR").create();
		StockItem stockItem = new StockItem("USB iphone stand", unitPrice);
		LineItem item = new LineItem(3, stockItem);
		order.getOrderItems().add(item);
		
		IPricingStrategyFacory pricingFactory = new PricingStrategyCreator();
		pricingFactory.createPricingStrategy(PricingStrategy.FREE_SAMPLE);
		item.getTotalPrice(new FreeSamplePricingStrategy());
		assertThat(item.getTotalPrice().toString(), is("EUR 1.3"));	
	}
	
	@Test
	public void makeVolumePricingStrategyTest() {
		Order order = new Order();
		MonetaryAmount unitPrice = Monetary.getDefaultAmountFactory().setNumber(12.5).setCurrency("EUR").create();
		MonetaryAmount threeUsbprice = Monetary.getDefaultAmountFactory().setNumber(28).setCurrency("EUR").create();
		StockItem stockItem = new StockItem("USB iphone stand", unitPrice);
		LineItem item = new LineItem(5, stockItem);
		order.getOrderItems().add(item);
		
		IPricingStrategyFacory pricingFactory = new PricingStrategyCreator();
		pricingFactory.createPricingStrategy(PricingStrategy.VOLUME_DISCOUNT);
		item.getTotalPrice(new VolumeDiscountPricingStrategy(threeUsbprice, 3));
		assertThat(item.getTotalPrice().toString(), is("EUR 53"));	
	}
	
	@Test
	public void makeWeightPricingStrategyTest() {
		//TODO
	}
}
