package fr.sgcib.exercice.kata.supermarketPricing.model;

import javax.money.MonetaryAmount;

import fr.sgcib.exercice.kata.supermarketPricing.business.IPricingStrategy;

public class LineItem {
	private int quantity;
	private MonetaryAmount totalPrice;
	private StockItem stockItem;

	public LineItem(int quantity, StockItem stockItem) {
		if (quantity < 1) { 
			throw new IllegalArgumentException("Number of units is invalid: "+quantity);
		}
		this.quantity = quantity;
		this.stockItem = stockItem;

	}
	public StockItem getStockItem() {
		return stockItem;
	}
	public void setStockItem(StockItem stockItem) {
		this.stockItem = stockItem;
	}
	public MonetaryAmount getTotalPrice() {
		return totalPrice;
	}
	public MonetaryAmount getTotalPrice(IPricingStrategy method) {
		totalPrice = method.getLinePrice(quantity, stockItem.getUnitCost());
		return totalPrice;
	}
}
