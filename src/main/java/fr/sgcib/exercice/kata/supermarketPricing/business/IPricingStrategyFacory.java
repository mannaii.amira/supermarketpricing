package fr.sgcib.exercice.kata.supermarketPricing.business;

import fr.sgcib.exercice.kata.supermarketPricing.constants.PricingStrategy;

public interface IPricingStrategyFacory {
	public IPricingStrategy createPricingStrategy(PricingStrategy strategyType);
}
