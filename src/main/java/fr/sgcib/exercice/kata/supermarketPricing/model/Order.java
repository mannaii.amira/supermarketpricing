package fr.sgcib.exercice.kata.supermarketPricing.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
	private LocalDate orderDate;
	private String orderNumber;
	List <LineItem> orderItems = new ArrayList<LineItem>();
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public void setOrderItems(List<LineItem> orderItems) {
		this.orderItems = orderItems;
	}
	public List<LineItem> getOrderItems() {
		return orderItems;
	}
}
