package fr.sgcib.exercice.kata.supermarketPricing.business;

import fr.sgcib.exercice.kata.supermarketPricing.constants.PricingStrategy;

public class PricingStrategyCreator implements IPricingStrategyFacory {

	public IPricingStrategy createPricingStrategy(PricingStrategy strategyType) {
		IPricingStrategy pricingStrategy = null;
		if(strategyType == null){
			return null;
		}		
		switch(strategyType)
		{
		  case UNIT: 
			pricingStrategy = new StandardPricingStrategy();
			break;
		  case WEIGHT: 
			pricingStrategy = new WeightPricingStrategy();
			break;
		  case FREE_SAMPLE: 
			pricingStrategy = new FreeSamplePricingStrategy();
		    break;
		  case VOLUME_DISCOUNT: 
			pricingStrategy = new VolumeDiscountPricingStrategy();
			break;
		  default:
			throw new IllegalArgumentException("Strategy type not found!");
		}
		return pricingStrategy;
	}

}
