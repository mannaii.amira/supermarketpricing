package fr.sgcib.exercice.kata.supermarketPricing.business;

import javax.money.MonetaryAmount;

public interface IPricingStrategy {
	public MonetaryAmount getLinePrice(int quantity, MonetaryAmount unitCost);
}
