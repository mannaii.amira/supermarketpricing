package fr.sgcib.exercice.kata.supermarketPricing.constants;

public enum PricingStrategy {
	UNIT,
	WEIGHT,
	FREE_SAMPLE,
	VOLUME_DISCOUNT,
	PERCENTAGE_DISCOUNT
}
