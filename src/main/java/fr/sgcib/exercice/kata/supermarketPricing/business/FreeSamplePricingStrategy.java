package fr.sgcib.exercice.kata.supermarketPricing.business;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

public class FreeSamplePricingStrategy implements IPricingStrategy {
	
	/**
	 * For the Bye tow get one for free
	 * here's the pattern used: (num / 3) * 2 * unitPrice + (num % 3) * unitPrice
	 */
	public MonetaryAmount getLinePrice(int quantity, MonetaryAmount unitCost) {
		MonetaryAmount part1 = unitCost.multiply((quantity / 3) * 2);
		int part2 = (quantity % 3);
		MonetaryAmount part2M = Monetary.getDefaultAmountFactory().setNumber(part2).setCurrency("EUR").create();

		return part1.add(part2M);
	}

}
