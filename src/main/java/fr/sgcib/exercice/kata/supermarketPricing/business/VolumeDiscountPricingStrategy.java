package fr.sgcib.exercice.kata.supermarketPricing.business;

import javax.money.MonetaryAmount;

public class VolumeDiscountPricingStrategy implements IPricingStrategy {

	private MonetaryAmount discountedVolumePrice;
	private int discountedVolume;

	public VolumeDiscountPricingStrategy() {
		super();
	}
	
	public VolumeDiscountPricingStrategy(MonetaryAmount discountedVolumePrice, int discountedVolume) {
		this.discountedVolumePrice = discountedVolumePrice;
		this.discountedVolume = discountedVolume;
	}

	public MonetaryAmount getLinePrice(int quantity, MonetaryAmount unitCost) {
		MonetaryAmount standardPrice =  unitCost.multiply(quantity);
		MonetaryAmount volumeDiscount = null;
		if (quantity >= discountedVolume) {
			volumeDiscount = (unitCost.multiply(discountedVolume))
					.subtract(discountedVolumePrice);
		}

		return standardPrice.subtract(volumeDiscount);
	}

}
