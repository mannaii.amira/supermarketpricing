package fr.sgcib.exercice.kata.supermarketPricing.business;

import javax.money.MonetaryAmount;

public class StandardPricingStrategy implements IPricingStrategy {
	
	public MonetaryAmount getLinePrice(int quantity, MonetaryAmount unitCost) {
		return unitCost.multiply(quantity);
	}

}
