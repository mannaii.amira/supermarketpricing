package fr.sgcib.exercice.kata.supermarketPricing.model;

import javax.money.MonetaryAmount;

public class StockItem {
	private String title;
	private MonetaryAmount unitCost;

	public StockItem(String title, MonetaryAmount unitCost) {
		if (unitCost == null || (unitCost.isNegativeOrZero())) { 
			throw new IllegalArgumentException("Unit price is invalid: "+unitCost.getNumber());
		}

		this.title = title;
		this.unitCost = unitCost;
	}

	public MonetaryAmount getUnitCost() {
		return unitCost;
	}
}
